function getProducts() {
    const url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            //console.log(request.responseText);
            let { value } = JSON.parse(request.responseText);
            processProducts(value);
        }
    }
    request.open("GET", url, true);
    request.send();
}
function processProducts(data) {
    var divTabla = document.getElementById("divTableProducts");
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    table.classList.add("table");
    table.classList.add("table-striped");
    // create headers
    var rowTh = document.createElement("tr");
    var header1 = document.createElement("th");
    header1.innerText = "Product Name";
    var header2 = document.createElement("th");
    header2.innerText = "Unit Price";
    var header3 = document.createElement("th");
    header3.innerText = "Units Stock";
    rowTh.appendChild(header1);
    rowTh.appendChild(header2);
    rowTh.appendChild(header3);
    table.appendChild(rowTh);

    // add rows
    data.forEach(element => {
        var row = document.createElement("tr");

        var columnName = document.createElement("td");
        columnName.innerText = element.ProductName;

        var columnPrice = document.createElement("td");
        columnPrice.innerText = element.UnitPrice;

        var columnStock = document.createElement("td");
        columnStock.innerText = element.UnitsInStock;

        row.appendChild(columnName);
        row.appendChild(columnPrice);
        row.appendChild(columnStock);

        tbody.appendChild(row);
    });

    table.appendChild(tbody);
    divTabla.appendChild(table);
}