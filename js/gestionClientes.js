function getCustomers() {
    const url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            //console.log(request.responseText);
            let { value } = JSON.parse(request.responseText);
            processCustomers(value);
        }
    }
    request.open("GET", url, true);
    request.send();
}

function processCustomers(data) {
    var divTabla = document.getElementById("divTableCustomers");
    var table = document.createElement('table');
    var tbody = document.createElement('tbody');

    table.classList.add("table");
    table.classList.add("table-striped");
    // create headers
    var rowTh = document.createElement("tr");
    var header1 = document.createElement("th");
    header1.innerText = "Contact Name";
    var header2 = document.createElement("th");
    header2.innerText = "Contact Title";
    var header3 = document.createElement("th");
    header3.innerText = "Company Name";
    var header4 = document.createElement("th");
    header4.innerText = "Country";
    rowTh.appendChild(header1);
    rowTh.appendChild(header2);
    rowTh.appendChild(header3);
    rowTh.appendChild(header4);
    table.appendChild(rowTh);

    // add rows
    data.forEach(element => {
        var row = document.createElement("tr");

        var column1 = document.createElement("td");
        column1.innerText = element.ContactName;

        var column2 = document.createElement("td");
        column2.innerText = element.ContactTitle;

        var column3 = document.createElement("td");
        column3.innerText = element.CompanyName;

        var column4 = document.createElement("td");
        var img4 = document.createElement("img");
        if(element.Country === 'UK'){
            element.Country = "United-Kingdom";
        }
        img4.src = `https://www.countries-ofthe-world.com/flags-normal/flag-of-${element.Country}.png`;
        img4.classList.add("flag");
        column4.appendChild(img4);

        row.appendChild(column1);
        row.appendChild(column2);
        row.appendChild(column3);
        row.appendChild(column4);

        tbody.appendChild(row);
    });

    table.appendChild(tbody);
    divTabla.appendChild(table);
}